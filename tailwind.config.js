module.exports = {
    separator: "-",
    corePlugins: {
        container: false,
    },

    theme: {
        colors: {
            'transparent': 'transparent',
            'white': '#fff',
            'black': '#000',
            'yellow': '#FEBD12',
            'grey-dark':'#575757',
            'grey': '#7A7F7F',
            'alert': {
                'danger': '#FF3B6A',
                'success': '#408d78',
                'warn':'#FF943B',
            },

        },
        fontFamily: {
            sans: [
                '"Roboto"',
                '-apple-system',
                'Arial',
                'sans-serif',
            ],
        },
        spacing: {
            '0': '0',
            '5': '0.313rem',
            '10': '0.625rem',
            '15': '0.938rem',
            '20': '1.25rem',
            '25': '1.563rem',
            '30': '1.875rem',
            '35': '2.188rem',
            '40': '2.5rem',
            '45': '2.813rem',
            '50': '3.125rem',
            '55': '3.438rem',
            '60': '3.75rem',
            '65': '4.063rem',
            '70': '4.375rem',
            '75': '4.688rem',
            '80': '5rem',
            '85': '5.313rem',
            '90': '5.625rem',
            '95': '5.938rem',
            '100': '6.25rem',
            '120': '7.5rem',
        },
        fontSize: {
            '11':'0.688rem',
            '12':'0.75rem',
            '14':'0.875rem',
            '16':'1rem',
            '18':'1.125rem',
            '20':'1.25rem',
            '22':'1.375rem',
            '24':'1.5rem',
            '26':'1.625rem',
            '28':'1.75rem',
            '30':'1.875rem',
            '32':'2rem',
            '35':'2.1875rem',
            '42':'2.625rem',
            '50':'3.125rem',
        }
    }
}
