"use strict";

import { gulp, $, server } from "./gulp/plugins"

import { config } from "./gulp/config"
import { style } from "./gulp/tasks/style"
import { iconfont } from "./gulp/tasks/iconfont"


function localserver() {
    server.init({
        proxy: "steelmag.test"
    });
}
function serve(done) {
    server.init({
        proxy: "steelmag.test",
        open: "local",
        reloadOnRestart: true,
    });
    done();
}


function watch() {
    gulp.watch(config.style.src, style);
    gulp.watch(config.icon.src, iconfont)
}


module.exports = {
    style: style,
    iconfont: iconfont,
    icons: gulp.series(iconfont, style),
    watch: gulp.series(style, watch),
    'watch:bs': gulp.series(style, gulp.series(serve, watch))
}
