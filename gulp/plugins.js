import gulp from "gulp"
import plugins from "gulp-load-plugins"
const $ = plugins({lazy: true})

// browsersync
import browserSync from "browser-sync";
const server = browserSync.create()


export { gulp, $, server }
