// =========================================================
// Gulp Task: Style
// Description:
// Dependencies: gulp-sass gulp-sourcemaps
// =========================================================
import { config } from '../config';
import { gulp, $, server } from '../plugins';

import autoprefixer from "autoprefixer"
import cssnano from "cssnano"
import tailwindcss from "tailwindcss"

export function style() {
    var plugins = [
        tailwindcss,
        autoprefixer,
        cssnano
    ];
    var stream =
// -------------------------------------------- Start Task
        gulp.src(config.style.src)
            .pipe($.sourcemaps.init())
            .pipe($.sass({
                    includePaths: ['./node_modules', '../'],
                })
            )
            .on("error", $.notify.onError({
                title: "Gulp",
                subtitle: "Failure!",
                message: "Error: <%= error.message %>",
                sound: "Basso"
            }))
            .pipe($.postcss(plugins))
            .pipe($.notify({
                title: "Gulp",
                subtitle: "Success!",
                message: 'Styles compiled successfully',
                onLast: true,
                sound: "Pop"
            }))

            .pipe($.sourcemaps.write(config.style.sourcemap))
            .pipe(gulp.dest(config.style.dest))
            .pipe(server.stream());
// ---------------------------------------------- End Task
    return stream;
};
