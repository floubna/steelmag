const infos = {
    vendorName: 'Steelmag',
    themeName: 'Steelmag',
}

const paths = {
    assets: `./app/design/frontend/${infos.vendorName}/${infos.themeName}/web`,
    assetsSrc: `./app/design/frontend/${infos.vendorName}/${infos.themeName}/web/src`
}

export const config = {
    style:  {
        src: `${paths.assetsSrc}/scss/**/**/*.scss`,
        dest: `${paths.assets}/css/`,
        sourcemap: `maps/`,
    },

    icon: {
        name: 'steelmag_icons',
        className: 'icon',
        src: `${paths.assetsSrc}/images/_icons/**/**/*.svg`,
        cssToFont: '../fonts/icons/',

        font: {
            dest: `${paths.assets}/fonts/icons/`,
            templates: {
                style: {
                    src: `${paths.assetsSrc}/templates/icons/style.scss`,
                    dest: `${paths.assetsSrc}/scss/bases/`,
                    fileName: "_icons"
                },

                vars: {
                    src: `${paths.assetsSrc}/templates/icons/vars.scss`,
                    dest: `${paths.assetsSrc}/scss/utilities/`,
                    fileName: "_icons"
                }
            }
        }
    },

    watch: {
        style: `${paths.assetsSrc}/scss/**/**/*.scss`
    }
}


