STEELMAG - TEST FRONT (Magento 2)
--- 
## Informations
- Thème parent : Luma (les feuilles de style d'origine sont désactivées)
- Développeur FRONT : Loubna Fattouh
- Version de Gulp : 4.0.2
- Version de Yarn : 1.22.4


### Choix
#### Général
- J'ai choisi d'utiliser gulp pour la création des tâches et scss pour le style. 
Contrairement à Grunt/less, celui-ci nous permet d'aller plus loin en séparent les feuilles de styles par pages afin d'optimiser les performances du site.
- Gulp me permet aussi de générer ma propre iconfont à partir des fichiers svg. 
- J'ai utilisé luma comme thème parent en supprimant toutes les feuilles de style de celui-ci. En effet, j'aime maîtriser le css présent sur le site et le style de luma ne respecte pas toujours les bonnes pratiques (Exemple: pas de Mobile-First).
#### Homepage
- J'ai utilisé Block CMS pour la création les blocs de la page d'acceuil. Etant donnée que ce contenu est amené à être administrer, j'ai fais en sorte d'utiliser une structure minim en se basant seulement sur les balises et non des classes. Les blocs sont appelés sur la HP en utilisant Layout update.
- En temps normal, ce genre de block administrable nécessite un dev back. Le mieux est de créer un type de block avec des champs spécifiques et nécéssaire pour limiter au maximum les effets de bords.
- Pour la bannière, avoir deux champs images (desktop/mobile) est la meilleure solution pour utiliser des images responsive et ainsi gagner des points en performance (surtout en mobile).

PS: Sur la maquette, il y avait plusieurs polices utilisées. J'ai limité à Roboto et IBM Plex Sans Condensed... Utiliser plusieurs polices custom n'est pas recommandé :)
## Préalable

- Installation de [NodeJs](https://nodejs.org/en/) @V - 10.16.0
- Installation de Yarn
    - [MacOs](https://yarnpkg.com/fr/docs/install#mac-stable)
    - [Debian](https://yarnpkg.com/fr/docs/install#debian-stable)
    - [Windows](https://yarnpkg.com/fr/docs/install#windows-stable)
    - [Autres](https://yarnpkg.com/fr/docs/install#alternatives-stable)



---
> L'ensemble des commandes ```yarn **``` seront lancé directement à la racine du projet
##  Installation

Le fonctionnement du thème repose sur l'installation des différents packages permettant de faire fonctionner 
convenablement les tâches GULP. Pour cela, toujours à la racine du projet, il suffit de lancer la commande : 
```
yarn install
```

##Développement

Lancer la commande permettant de surveiller tous les fichiers _.scss_ et lance automatiquement la compilation.
Cette commande permettera aussi de créer la font icons automatiquement en cas d'ajout. Étant 
donné que la tâche tourne en tâche de fond, elle se relancera à chaque enregistrement d'un fichier source !

```javascript
yarn watch
```

**ATTENTION : vérifier qu'il n'y ai aucune erreurs ou avertissements avant de continuer**

> BrowserSync est un module qui va vous permettre d'actualiser et de synchroniser automatiquement vos pages web pour un 
développement plus rapide. Il se déclanche avec la tâche ```yarn watch:bs```. À chaque modification d'un fichier scss, 
il va mettre à jour le navigateur automatiquement. Ce qui va permettre de gagner du temps en développement.
Pour le configurer, il faut aller dans **./gulp/configs.js** et modifier l'url de votre local dans l'objet website

**Attention à ne pas commit cette modification. Le mieux est d'avoir la même url que toute l'équipe ici : http://steelmag.test**

---
## Comment dois-je organiser mes fichiers sources ?

#### 1 | Procédure d'ajout d'une nouvelle feuille de style (page)

**Exemple avec l'ajout de la feuille de style home.scss**

1. On se place dans **./src/scss/pages**
2. On ajoute la feuille de style home.scss
3. En haut de page on place les lignes si dessous, afin d'appeler les variables et les mixins:

    `````
    @import "../imports";
    `````

    Pour infos, Le fichier \_imports.scss se trouve à la racine des sources scss. il faut bien faire attention à ce que le chemin vers ce fichier soit correct.

4. Appeler la feuille de style via le xml qui correspond à la page


> MIXINS
>
> Il est possible de rajouter vos propres mixins. Pour cela, il faut les rajouter dans web/src/scss/utilities/_mixins.scss.
Ensuite pour les utiliser, il suffit de les importer via le fichier web/src/scss/_imports.scss 

####2 | Les fonts icons

- Préparer les icons en faisant attention à ne pas utiliser des svg avec des contours. 
    > Il est possible de vectoriser le contour, facilement, sur illustrator

- Placer les icons svg dans le dossier web/src/images/_icons/

- Si la tâche ```yarn watch``` tourne en tâche de fond au moment de l'ajout des icons, vous pouvez passer la prochaine étape alors pas besoin de faire le point 5

- Re-générer la police en lançant la commande ```yarn icons```

Le script nous génère la police avec des classes du genre: ```.icon-{nomIconSVG}```. Mais aussi des variables du genre 
```$icon-{nomIconSVG}``` . 
Pour l'utilisation des classes, ajouter les classes suivante à votre élément html: ```.icon .icon-{nomIconSVG}``` 
Pour l'utilisation des variables, dans votre css, il est possible d'utiliser le mixins icon

```css
.monElement {

  &::after {
    @include icon($icon-nomIconSVG);
  }
}
```
Attention les noms des fichiers svg doivent être unique

